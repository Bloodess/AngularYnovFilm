import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RechercherFilmsComponent } from './rechercher-films.component';

describe('RechercherFilmsComponent', () => {
  let component: RechercherFilmsComponent;
  let fixture: ComponentFixture<RechercherFilmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RechercherFilmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RechercherFilmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

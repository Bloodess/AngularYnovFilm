import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'rechercher-films',
  templateUrl: './rechercher-films.component.html',
  styleUrls: ['./rechercher-films.component.css']
})
export class RechercherFilmsComponent {

 public title:string = 'Rechercher votre film';
 public bool:boolean = false;
 public titreRecherche:string = '';
 public listeRecherche:Object[];
 public listeFilm:Object[] = [

   {id:1, Title:"La ligne verte", Poster:"./assets/greenLine.jpg", Year:1994},
   {id:2, Title:"Inception", Poster:"./assets/in.jpg", Year:2015},
   {id:3, Title:"Braveheart", Poster:"./assets/brave.jpg", Year:1984}
 ];

 rechercher() {
 	 this.listeRecherche = [];
 	for(let item of this.listeFilm){
 		if(item['Title'].toUpperCase() === this.titreRecherche.toUpperCase()){
 			this.listeRecherche.push(item);
 		}
 	}


 	// if (this.bool == false) {
 	// 	this.bool = true;
 	// }
 	// else if (this.bool == true) {
 	// 	this.bool = false;
 	// }
 }
}


import { FilmTmpPage } from './app.po';

describe('film-tmp App', () => {
  let page: FilmTmpPage;

  beforeEach(() => {
    page = new FilmTmpPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
